FROM php:7.4-apache

SHELL ["/bin/bash", "-c"]

RUN apt-get update

RUN apt-get install -y \
  libicu-dev \
  libonig-dev \
  openssl \
  ssl-cert

RUN apt-get install -y \
  autoconf \
  dnsutils \
  make \
  g++ \
  gcc

RUN apt-get install -y \
  git \
  unzip \
  wget \
  vim

RUN apt-get install -y \
  libzip-dev \
  && docker-php-ext-install zip

## GD
#RUN apt-get install -y \
#libjpeg-dev \
#libfreetype6-dev
#RUN apt-get install -y \
#libmagick++-dev \
#libmagickwand-dev \
#libpq-dev \
#libfreetype6-dev \
#libjpeg62-turbo-dev \
#libpng-dev \
#libwebp-dev \
#libxpm-dev

RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*

# 追加mod
RUN a2enmod ssl rewrite
# 排除mod
RUN a2dismod autoindex -f

# php 追加モジュールインストール
# ENV_PHP_XDEBUGがTRUEならインストール
ARG ENV_PHP_XDEBUG
RUN docker-php-ext-install -j$(nproc) iconv intl mbstring pdo_mysql opcache
RUN pecl install mailparse && \
([[ ! "${ENV_PHP_XDEBUG:-false}" =~ ^([Tt]rue|TRUE)$ ]] || pecl install xdebug)
RUN docker-php-ext-enable mailparse && \
([[ ! "${ENV_PHP_XDEBUG:-false}" =~ ^([Tt]rue|TRUE)$ ]] || docker-php-ext-enable xdebug)

## GD
#RUN docker-php-ext-install -j$(nproc) gd

# セキュリティ対応
WORKDIR /etc/apache2
RUN sed -i \
  -e 's/^ServerTokens OS/ServerTokens Prod/' \
  conf-available/security.conf

RUN sed -i \
  -e 's/Options Indexes FollowSymLinks/Options FollowSymLinks/' \
  apache2.conf

# extensionのディレクトリ（/usr/src/ext）を有効化
RUN docker-php-source extract

# docker-php-ext-install用にphpredisを取得
#RUN git clone -b 5.0.2 --depth 1 https://github.com/phpredis/phpredis.git /usr/src/php/ext/redis
#RUN docker-php-ext-install redis

# 必要な時に中で composer 使えるようにしておきます。
COPY ./docker/php-apache/get_comporser.sh /usr/local/src/
WORKDIR /usr/local/src
RUN sh ./get_comporser.sh
RUN mv ./composer.phar /usr/local/bin/composer

# サイト設定
COPY ./docker/php-apache/vhosts.conf /etc/apache2/sites-available/
COPY ./key/local-server.crt /etc/ssl/certs/
COPY ./key/local-server.key /etc/ssl/private/
RUN  a2ensite vhosts

# php設定追加
COPY ./docker/php-apache/php.ini $PHP_INI_DIR/
COPY ./docker/php-apache/xdebug.ini $PHP_INI_DIR/conf.d/
COPY ./docker/php-apache/overrides.ini $PHP_INI_DIR/conf.d/

WORKDIR /var/www/html
