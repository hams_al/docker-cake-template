# Docker-CakePHP #


## ファイル構成 ##
```
doker-cake-template
　├ cms
　│　├ config
　│　│　└ app.php
　│　├ db
　│　│　├ migrations
　│　│　└ seeds
　│　├ src
　│　│　├ Controller
　│　│　├ Model
　│　│　├ Template
　│　│　└ View
　│　├ webroot
　│　│　└ index.php
　│　├ composer.json
　│　├ composer.lock
　│　└ phinx.yml
　├ docker
　│　├ mysql
　│　│　├ Dockerfile
　│　│　└ my.cnf
　│　├ options
　│　└ php-apache
　│　 　├ Dockerfile
　│　 　└ php.ini
　├ key
　├ .env
　└ docker-compose.yml
```
※.envファイルの設置を忘れずに。

```
APP_NAME=Docker-Cake
APP_MODE=develop
TZ=Asia/Tokyo

MYSQL_HOST=local-db
MYSQL_DATABASE=cms_db
MYSQL_USER=cms_admin
MYSQL_PASSWORD=Local3gx1txbR0zWUTHwp7hTL5w6Y
MYSQL_ROOT_PASSWORD=LocalaycfCbleCMhumNQuaOaiU4ft

PHINX_DB_HOST="${MYSQL_HOST}"
PHINX_DB_NAME="${MYSQL_DATABASE}"
PHINX_DB_USER="${MYSQL_USER}"
PHINX_DB_PASSWORD="${MYSQL_PASSWORD}"

PMA_ARBITRARY=1
PMA_HOST="${MYSQL_HOST}"
PMA_USER="${MYSQL_USER}"
PMA_PASSWORD="${MYSQL_PASSWORD}"
```

---

## 開発環境構築手順 ##
1. .envファイルサンプルをコピーし、「.env」を作成する
2. Dockerイメージを作成する
3. コンテナを起動する
4. ComposerでVendorファイルをインストールする
5. データベースのマイグレーションをする

※以下のコマンド類を参考にしてください

---

## コマンド類 ##

### Dockerイメージの作成 ###

※phpとmysqlがビルドされる
```
$ docker-compose build
```

※Xdebugを有効にするとき
```
$ docker-compose -f docker-compose.yml -f docker/options/enable-xdebug.yml build
```

### 作成したイメージの確認 ###

※dockerコマンドであることに注意
```
$ docker images
```


### コンテナの起動 ###

※phpMyAdminのイメージがダウンロードされる。

```
$ docker-compose up -d
```

### 起動確認 ###

```
$ docker-compose ps
```

### ブラウザから確認 ###

※mac環境でSSLの証明書エラーが出る場合は、httpで確認。

```
https://localhost/info.php
http://localhost:8080/
```


### 停止 ###

```
$ docker-compose stop
```

---

### Composerを使ってVendorファイルのインストール ###

※composer.json、composer.lockを確認
```
$ docker-compose exec web composer install
```

※CakePHP初期画面表示
```
https://localhost/
```

### Migration ###
* マイグレーション
    ```
    $ docker-compose exec web php vendor/bin/phinx migrate
    ```
* 初期値投入
    ```
    $ docker-compose exec web php vendor/bin/phinx seed:run
    ```
* ロールバック
    ```
    $ docker-compose exec web php vendor/bin/phinx rollback
    ```


---

### PHPコンテナに入って環境変数を確認 ###

```
$ docker-compose exec web /bin/sh
# printenv APP_NAME
```

### MySQLコンテナに入ってdump取得 ###

```
$ docker-compose exec local-db /bin/sh
# mysqldump -u cms_admin -p --single-transaction --databases cms_db > /tmp/cms_db.sql
```

---

### キャッシュクリア ###

```
$ docker-compose exec web bin/cake cache clear_all
```

---

### 再ビルド手順（作成した環境を初期化） ###

1. composerキャッシュクリア

    ```
    $ docker-compose exec web composer clear-cache
    ```

2. コンテナ、イメージ、ボリューム、ネットワークの削除

    ```
    $ docker-compose down --rmi all --volumes
    ```
   
3. 残りのイメージ削除

    ※他案件で使ってるイメージも削除されるので注意。
    無理に消す必要はないかも。。
    
    ```
    $ docker rmi -f $(docker images -aq)
    ```
    
    以下のコマンドでIMAGE IDを特定して消す方法も。  
    
    ```
    $ docker images
    
    ex)
    IMAGE ID
    b230c73cd1db
    add3b2118f18
    642ac79dc240
    
    $ docker rmi b230c73cd1db 642ac79dc240 -f
    ```

4. /cms/vendorフォルダの削除

5. ビルド

---

### オプション指定のビルド ###

※XDEBUGを有効にするとき

```
docker-compose -f docker-compose.yml -f docker/options/enable-xdebug.yml build
```