<?php

namespace Migrations;

use Cake\I18n\FrozenTime;
use Phinx\Seed\AbstractSeed;

class User extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run() {
        $this->execute('TRUNCATE TABLE users');
        $now = FrozenTime::now()->toDateTimeString();
        $users = [
            [
                'loginid'     => 'cms-admin',
                'password'    => '$2y$10$.4Gh0RNHm8gkMVrD.qcw/uPuLJOMTN7NFN8Pp.a2PDlKQEDtF8WaS',
                'name'        => 'システム管理者',
                'kana'        => 'システムカンリシャ',
                'mail'        => 'cms_admin@example.com',
                //'suspend_flg' => 0,
                //'note'        => '開発用の顧客になります',
                'created'     => $now,
                'modified'    => $now,
            ],
        ];
        $client = $this->table('users');
        $client->insert($users)
            ->save();
    }
}
