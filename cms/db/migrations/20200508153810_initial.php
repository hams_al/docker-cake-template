<?php
namespace Migrations;

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class Initial extends AbstractMigration
{

    public function up()
    {
        $this->execute("
CREATE TABLE IF NOT EXISTS `users` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ユーザーID',
  `loginid` VARCHAR(32) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_bin' NULL COMMENT 'ログインID',
  `password` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_bin' NULL COMMENT 'パスワード',
  `name` VARCHAR(32) NULL COMMENT '氏名',
  `kana` VARCHAR(32) NULL COMMENT 'フリガナ',
  `mail` VARCHAR(128) NULL COMMENT 'メールアドレス',
  `suspend_flg` TINYINT(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '利用停止フラグ',
  `note` TEXT NULL COMMENT '備考',
  `created` DATETIME NULL COMMENT '登録日時',
  `modified` DATETIME NULL COMMENT '更新日時',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `loginid_UNIQUE` (`loginid` ASC),
  UNIQUE INDEX `mail_UNIQUE` (`mail` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_general_ci
COMMENT = 'ユーザー';
");
    }

    public function down()
    {
        $this->execute("
DROP TABLE `users`;
        ");
    }
}
