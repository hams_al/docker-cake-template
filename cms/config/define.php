<?php
/**
 * 定数定義ファイル
 *
 * アプリケーション全体で使用する
 * 定数等をここで定義する
 */

// App Modes
define('APP_MODE_MASTER', 'master');
define('APP_MODE_DEVELOP', 'develop');
define('APP_MODE_LOCAL', 'local');

// App Mode
define('APP_MODE', env('APP_MODE'));

// App Debug Env
switch (APP_MODE) {
    case APP_MODE_MASTER:
        define('APP_DEBUG_ENV', false);
        break;
    case APP_MODE_DEVELOP:
    case APP_MODE_LOCAL:
        define('APP_DEBUG_ENV', true);
        break;
}

// Routes
define('APP_ROUTE_WEB', 'web');
define('APP_ROUTE_ADMIN', 'admin');
$GLOBALS['APP_ROUTES'] = [
    APP_ROUTE_WEB,
    APP_ROUTE_ADMIN,
];
